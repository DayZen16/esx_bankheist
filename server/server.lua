local alarmTriggered = false
local unlockedDoors = {}
local bankMoney = Config.BankMoney

ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

function math.randomchoice(t) --Selects a random item from a table
    local keys = {}
    for key, value in ipairs(t) do
        keys[#keys+1] = key --Store keys in another table
    end
    index = keys[math.random(1, #keys)]
    return t[index]
end

function getPoliceOnline()
  local xPlayers = ESX.GetPlayers()
  local cops = 0

  for i=1, #xPlayers, 1 do
    local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
    if xPlayer.job.name == 'police' then
      cops = cops + 1
    end
  end

  return cops
end

Code = math.random(111111,999999)
--print("\nCode is " .. Code)
ESX.RegisterServerCallback('esx_bankHeist:getCode', function(source, cb)
	cb(Code)
	print("\nSent " .. Code .. " to callback")
end)

ESX.RegisterServerCallback('esx_bankHeist:isDoorUnlocked', function(source, cb, door)
	cb(table.contains(unlockedDoors,door))--will return false if door has not been unlocked
end)


RegisterServerEvent('esx_bankHeist:hackingDone')
AddEventHandler('esx_bankHeist:hackingDone', function()
  TriggerClientEvent('esx:showNotification', source, _U('get_code',Code))
end)

RegisterServerEvent('esx_bankHeist:OpenVaultServer')
AddEventHandler('esx_bankHeist:OpenVaultServer', function(hash, coords)
	--print("\nhash: " .. hash .. " coords: " .. coords)
	TriggerClientEvent('esx_bankHeist:OpenVaultClient', -1, hash, coords)
end)

RegisterServerEvent('esx_bankHeist:CloseVaultServer')
AddEventHandler('esx_bankHeist:CloseVaultServer', function(hash, coords)
	--print("\nhash: " .. hash .. " coords: " .. coords)
	TriggerClientEvent('esx_bankHeist:CloseVaultClient', -1, hash, coords)
end)

RegisterServerEvent('esx_bankHeist:lookupCode')
AddEventHandler('esx_bankHeist:lookupCode', function()
  TriggerClientEvent('esx:showNotification', source, _U('get_code',Code))
end)


RegisterServerEvent('esx_bankHeist:startHacking')
AddEventHandler('esx_bankHeist:startHacking', function()
	local xPlayer = ESX.GetPlayerFromId(source)

  --[[if getPoliceOnline() < 4 then
    TriggerClientEvent('esx:showNotification', source, _U('cops_needed',4))
    return
  end]]

  TriggerClientEvent('esx_bankHeist:startHacking', source)
end)


RegisterServerEvent('esx_bankHeist:triggerSilentAlarm')
AddEventHandler('esx_bankHeist:triggerSilentAlarm', function(coords)
	local xPlayers = ESX.GetPlayers()

	if alarmTriggered then
		TriggerClientEvent('esx:showNotification', source, 'The alarm has already been triggered')
	else
		TriggerClientEvent('esx:showNotification', source, 'The silent alarm has been triggered')

		for i=1, #xPlayers, 1 do
			local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
			if xPlayer.job.name == 'police' then
				TriggerClientEvent('esx:showNotification', xPlayers[i], _U('rob_in_prog', 'Robbery', 'Pacific Standard Bank'))
				TriggerClientEvent('esx_bankHeist:setblip', xPlayers[i], coords)
			end
		end
	end
end)

RegisterServerEvent('esx_bankHeist:disableAlarm')
AddEventHandler('esx_bankHeist:disableAlarm', function()
	local xPlayers = ESX.GetPlayers()

	if not alarmTriggered then
		TriggerClientEvent('esx:showNotification', source, 'The alarm is already disabled')
	else
		TriggerClientEvent('esx:showNotification', source, 'The alarm has been disabled')

		for i=1, #xPlayers, 1 do
			local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
			if xPlayer.job.name == 'police' then
			  TriggerClientEvent('esx_bankHeist:killblip', xPlayers[i])
			end
		end

		TriggerClientEvent('esx_bankHeist:killAlarm', -1)
    alarmReset = true
    alarmTriggered = false
    bankMoney = Config.BankMoney
	end
end)

RegisterServerEvent('esx_bankHeist:heistStart')
AddEventHandler('esx_bankHeist:heistStart', function(coords)
	local xPlayer = ESX.GetPlayerFromId(source)

	--[[if alarmTriggered then
		TriggerClientEvent('esx:showNotification', source, "Robbery already in progress")
    return
  end]]

  --[[if getPoliceOnline() < 4 then
    TriggerClientEvent('esx:showNotification', source, _U('cops_needed',4))
    return
  end]]

  TriggerClientEvent('esx_bankHeist:heistStart', source)
  if not alarmTriggered then
  	TriggerClientEvent('esx_bankHeist:triggerAlarm', -1, coords)
    alarmTriggered = true

  	for i=1, #xPlayers, 1 do
      local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
      if xPlayer.job.name == 'police' and not alarmTriggered then
        TriggerClientEvent('esx:showNotification', xPlayers[i], _U('rob_in_prog', 'Robbery', 'Pacific Standard Bank'))
  			TriggerClientEvent('esx_bankHeist:setblip', xPlayers[i], coords)
      end
    end
  end
end)

RegisterServerEvent('esx_bankHeist:hack')
AddEventHandler('esx_bankHeist:hack', function(doorCfg)
	local source = source
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.getInventoryItem('keycard').count >= 1 then
		TriggerClientEvent('esx_bankHeist:startHack', source, doorCfg)
	end
end)

ESX.RegisterServerCallback('esx_bankHeist:canTakeMoney', function(source, cb)
  local xPlayers = ESX.GetPlayers()

  --local quantity = 1000 * #xPlayers
  local quantity = 10000
  if quantity <= bankMoney then
    cb(true)
  else
    cb(false)
  end
end)

ESX.RegisterServerCallback('esx_bankHeist:hasKeycard', function(source, cb)
  local source = source
	local xPlayer = ESX.GetPlayerFromId(source)

	if xPlayer.getInventoryItem('keycard').count >= 1 then
		cb(true)
	else
    cb(false)
  end
end)

RegisterServerEvent('esx_bankHeist:takeMoney')
AddEventHandler('esx_bankHeist:takeMoney', function()
  local xPlayer = ESX.GetPlayerFromId(source)
  local xPlayers = ESX.GetPlayers()
  --local quantity = 1000 * #xPlayers
  local quantity = 10000

  bankMoney = bankMoney - quantity
  xPlayer.addAccountMoney('black_money', quantity)
  TriggerClientEvent('esx:showNotification', source, _U('money_stolen',quantity))
end)
