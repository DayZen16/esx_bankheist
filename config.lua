Config                        = {}
Locales												= {}
Config.Locale                 = 'en'
Config.DrawDistance           = 100
Config.MarkerSize             = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor            = {r = 102, g = 102, b = 204}
Config.MarkerType             = 1
Config.timeToLockpick					= 10
Config.BankMoney							= 1000000 -- 1 millin dollars

Config.BankHeist = {
	MaxPlayer = 32,
	PoliceZones = {
		{x=255.90,y=222.40,z=105.29}
	},
	Zones = {
		ManagerPC = {x=261.48,y=204.76,z=109.29},
		TakeLocation = {x=265.11,y=213.82,z=100.68},
		Terminal1 = {x=253.3081,y=228.4226,z=100.733},
		Terminal2 = {x=261.952,y=223.122,z=105.334}
	},
	AlarmZones = {
		{x=253.24,y=223.39,z=105.29},
		{x=247.99,y=224.90,z=105.29},
		{x=242.86,y=226.78,z=105.29},
		{x=264.72,y=219.83,z=100.68},
		{x=251.86,y=216.91,z=100.68}
	},
	Doors = {
		SecDoor = {
			Heading = 250.0,
			Hash = 746855201,
			Pos = {x=261.81,y=221.86,z=105.28}
		},
		VaultDoor = {
			Heading = 160.0,
			Hash = 961976194,
			Pos = {x=254.12,y=224.32,z=100.88}
		}
	}
}
