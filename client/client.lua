local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX                           = nil
PlayerData                    = {}
--local creatingPed             = false
local LastPart                = nil
local HasAlreadyEnteredMarker = false
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local Code                    = nil
local AlarmSoundId            = nil
local robInProgress           = false
local spawnedNpcs             = {}
local Npcs = {
  {x=258.77, y=216.06, z=100.73, h=67.18},
  {x=231.91, y=211.56, z=104.50, h=113.60},
  {x=229.09, y=217.79, z=104.60, h=111.76},
  {x=263.97, y=216.13, z=105.33, h=159.05},
  {x=246.48, y=214.11, z=105.33, h=335.99},
  {x=259.91, y=217.45, z=105.33, h=68.73},
  {x=259.34, y=210.75, z=110.28, h=71.18}
}

DecorRegister('isSurrendering', 3) -- 3 for int
DecorRegister('isCuffed', 3) -- 3 for int


Citizen.CreateThread(function()
  while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()

  TriggerServerEvent('esx_bankHeist:getCode')

  createGuards()
end)

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

function clearArea()
  for _, npc in pairs(Npcs) do
    local coords = {x=npc.x, y=npc.y, z=npc.z}
    ClearArea(coords.x, coords.y, coords.z, 2.0, true, false, false, false);
    ClearAreaOfCops(coords.x, coords.y, coords.z, 2.0, 1)
    ClearAreaOfPeds(coords.x, coords.y, coords.z, 2.0, 1)
    ClearAreaOfEverything(coords.x, coords.y, coords.z, 2.0, true, true, true, true)
  end
end

function createGuards()
  clearArea()
  for _, npc in pairs(Npcs) do
    local coords = {x=npc.x, y=npc.y, z=npc.z}
    local hash = GetHashKey("mp_s_m_armoured_01")
    --if not DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 2.0, hash, 0) then
      TriggerEvent('esx_bankHeist:createGuard', coords, npc.h)
    --end
  end
end


function setCurrentAction(CurrentAction, CurrentActionMsg, CurrentActionData)
  CurrentAction = CurrentAction
  CurrentActionMsg = CurrentActionMsg
  CurrentActionData = CurrentActionData
  ESX.ShowHelpNotification(CurrentActionMsg)
end

function mycb(success, timeremaining)
  if success then
    local doorConfig = Config.BankHeist.Doors.VaultDoor
    local door = GetClosestObjectOfType(doorConfig.Pos.x, doorConfig.Pos.y, doorConfig.Pos.z, 25.0, doorConfig.Hash, 0, 0, 0)
    local CurrentHeading = GetEntityHeading(door)
    local coords = GetEntityCoords(door)
    local hash =  doorConfig.Hash
    print("\nhash: " .. hash)

    if hash == Config.BankHeist.Doors.VaultDoor.Hash then --vault door
      if 160.0 - round(CurrentHeading, 1) < 10 then -- the door's angle is close to 160 when closed
        --TriggerServerEvent('esx_bankHeist:UnlockDoorServer', hash, coords, door)
        TriggerServerEvent('InteractSound_SV:PlayWithinDistance', 20, 'vaultopen', 1.0)
        TriggerServerEvent('esx_bankHeist:OpenVaultServer', hash, coords)
      end
    elseif hash == Config.BankHeist.Doors.SecDoor.Hash then -- security door
      if 250.0 - round(CurrentHeading, 1) < 10 then -- the door's angle is close to 250.0 when closed
        TriggerServerEvent('esx_bankHeist:UnlockDoorServer', hash, coords, door)
        if PlayerData.job.name ~= 'banker' and PlayerData.job.name ~= 'police' then
          local playerPed = GetPlayerPed(-1)
          local PedPosition		= GetEntityCoords(playerPed)
          local PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z }
          --TriggerServerEvent('esx_addons_gcphone:startCall', 'police', 'A silent alarm was triggered at the Bank!', PlayerCoords, {PlayerCoords = { x = x, y = y, z = z }})
          TriggerServerEvent('esx_bankHeist:triggerSilentAlarm', PlayerCoords)
          SetPlayerWantedLevel(PlayerId(), 4, false)
          SetPlayerWantedLevelNow(PlayerId(), false)
        end
      end
    end

    print('Success with '..timeremaining..'s remaining.')
    TriggerEvent('mhacking:hide')
  else
    print('Failure')
    TriggerEvent('mhacking:hide')
  end
end


local entityEnumerator = {
  __gc = function(enum)
    if enum.destructor and enum.handle then
      enum.destructor(enum.handle)
    end
    enum.destructor = nil
    enum.handle = nil
  end
}

local function EnumerateEntities(initFunc, moveFunc, disposeFunc)
  return coroutine.wrap(function()
    local iter, id = initFunc()
    if not id or id == 0 then
      disposeFunc(iter)
      return
    end

    local enum = {handle = iter, destructor = disposeFunc}
    setmetatable(enum, entityEnumerator)

    local next = true
    repeat
      coroutine.yield(id)
      next, id = moveFunc(iter)
    until not next

    enum.destructor, enum.handle = nil, nil
    disposeFunc(iter)
  end)
end

function EnumeratePeds()
  return EnumerateEntities(FindFirstPed, FindNextPed, EndFindPed)
end

function alertPolice()
  local playerPed = GetPlayerPed(-1)
  local PedPosition		= GetEntityCoords(playerPed)
  local PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z }
  --TriggerServerEvent('esx_addons_gcphone:startCall', 'police', 'A silent alarm was triggered at the Bank!', PlayerCoords, {PlayerCoords = { x = x, y = y, z = z }})
  TriggerServerEvent('esx_bankHeist:triggerSilentAlarm', PlayerCoords)
end

function triggerCombat()
  local player = GetPlayerPed(-1)
  local playerCoords = GetEntityCoords(player)

  for ped in EnumeratePeds() do
    if ped ~= player then
      local pedCoords = GetEntityCoords(ped)
      local groupHash = GetPedRelationshipGroupHash(ped)
      local isCuffed = DecorGetInt(ped, 'isCuffed') == 1
      local isSurrendering = DecorGetInt(ped, 'isCuffed') == 1

      if not (isCuffed and isSurrendering) and (groupHash == GetHashKey("SECURITY_GUARD")) and HasEntityClearLosToEntity(ped, player, 17) then
        SetPedAlertness(ped, 3)
        TaskCombatPed(ped, player, 0, 16 )
        alertPolice()
      end
    end
  end
end


-- Display markers
Citizen.CreateThread(function()
  while true do
    if IsInteriorReady(139265) == false then
      Citizen.Wait(500)
    end

    local coords = GetEntityCoords(GetPlayerPed(-1))

    if PlayerData.job ~= nil then
      for k,pos in pairs(Config.BankHeist.Zones) do
        if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, 255, 255, 0, 100, false, true, 2, false, false, false, false)
        end
      end

      for k,pos in pairs(Config.BankHeist.AlarmZones) do
        if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) and PlayerData.job.name == 'banker' then
          DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, 255, 255, 0, 100, false, true, 2, false, false, false, false)
        end
      end

      for k,pos in pairs(Config.BankHeist.PoliceZones) do
        if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) and PlayerData.job.name == 'police' and robInProgress then
          DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, 0, 0, 255, 100, false, true, 2, false, false, false, false)
        end
      end
    else
      Citizen.Wait(500)
    end

    Citizen.Wait(0)
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(10)

    if PlayerData.job ~= nil then
      local coords          = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker      = false
      local currentPart     = nil

      for k,pos in pairs(Config.BankHeist.PoliceZones) do
        if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.MarkerSize.x) and PlayerData.job.name == 'police' and robInProgress then
          isInMarker      = true
          currentPart     = 'resetAlarm'
        end
      end

      for k,pos in pairs(Config.BankHeist.AlarmZones) do
        if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.MarkerSize.x) and PlayerData.job.name == 'banker' then
          isInMarker      = true
          currentPart     = 'silentAlarm'
        end
      end

      local TakeLocation = Config.BankHeist.Zones.TakeLocation
      if (GetDistanceBetweenCoords(coords, TakeLocation.x, TakeLocation.y, TakeLocation.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentPart     = 'takemoney'
      end

      local ManagerPC = Config.BankHeist.Zones.ManagerPC
      if (GetDistanceBetweenCoords(coords, ManagerPC.x, ManagerPC.y, ManagerPC.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentPart     = 'managerpc'
      end

      local Terminal1 = Config.BankHeist.Zones.Terminal1
      if (GetDistanceBetweenCoords(coords, Terminal1.x, Terminal1.y, Terminal1.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentPart     = 'terminal1'
      end

      local Terminal2 = Config.BankHeist.Zones.Terminal2
      if (GetDistanceBetweenCoords(coords, Terminal2.x, Terminal2.y, Terminal2.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentPart     = 'terminal2'
      end

      if isInMarker and not HasAlreadyEnteredMarker or (isInMarker and (LastPart ~= currentPart)) then
        HasAlreadyEnteredMarker = true
        LastPart                = currentPart

        print("\nEnter/Exit marker events currentPart: " .. currentPart)
        TriggerEvent('esx_bankHeist:hasEnteredMarker', currentPart)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_bankHeist:hasExitedMarker', LastPart)
      end
    end
  end
end)


-- Key controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(10)

    if CurrentAction ~= nil and PlayerData.job ~= nil then
      ESX.ShowHelpNotification(CurrentActionMsg)

      if IsControlJustReleased(0, Keys['E']) then
        local scenario = 'PROP_HUMAN_BUM_BIN'

        if CurrentAction == 'unlock_door' then
          local door = GetClosestObjectOfType(CurrentActionData.Pos.x, CurrentActionData.Pos.y, CurrentActionData.Pos.z, 5.0, CurrentActionData.Hash, 0, 0, 0)
          local coords = GetEntityCoords(door)
          TaskStartScenarioInPlace(GetPlayerPed(-1), scenario, 0, false)
          Wait(1000)
          ClearPedTasksImmediately(GetPlayerPed(-1))
          --TriggerServerEvent('esx_bankHeist:UnlockDoorServer', CurrentActionData.Hash, coords, door)
        end

        if CurrentAction == 'lock_door' then
          local door = GetClosestObjectOfType(CurrentActionData.Pos.x, CurrentActionData.Pos.y, CurrentActionData.Pos.z, 5.0, CurrentActionData.Hash, 0, 0, 0)
          local coords = GetEntityCoords(door)
          TaskStartScenarioInPlace(GetPlayerPed(-1), scenario, 0, false)
          Wait(1000)
          ClearPedTasksImmediately(GetPlayerPed(-1))
          --TriggerServerEvent('esx_bankHeist:LockDoorServer', CurrentActionData.Hash, coords, door)
        end

        if CurrentAction == 'reset_alarm' then
          TaskStartScenarioInPlace(GetPlayerPed(-1), scenario, 0, false)
          Wait(1000)
          ClearPedTasksImmediately(GetPlayerPed(-1))
          TriggerServerEvent('esx_bankHeist:disableAlarm')

          --[[for k,v in pairs(Config.BankHeist.Doors) do
            initDoor(v)
          end]]
        end

        if CurrentAction == 'silent_alarm' then
          TaskStartScenarioInPlace(GetPlayerPed(-1), scenario, 0, false)
          Wait(1000)
          ClearPedTasksImmediately(GetPlayerPed(-1))
          TriggerServerEvent('esx_bankHeist:triggerSilentAlarm', coords)
          alertPolice()
        end

        if CurrentAction == 'take_money' then
          local coords = GetEntityCoords(GetPlayerPed(-1))
          TriggerServerEvent('esx_bankHeist:heistStart', coords)
        end

        if CurrentAction == 'manager_pc' then
          TriggerServerEvent('esx_bankHeist:lookupCode')
        end
        if CurrentAction == 'manager_pc_hack' then
          TriggerServerEvent('esx_bankHeist:startHacking')
        end

        if CurrentAction == 'terminal1' then
          triggerCombat()
          enterCode(Config.BankHeist.Doors.VaultDoor)
        end

        if CurrentAction == 'terminal1_close' then
          local door = Config.BankHeist.Doors.VaultDoor
          local hash =  door.Hash
          local door  = GetClosestObjectOfType(door.Pos.x, door.Pos.y, door.Pos.z, 10.0, hash, 0, 0, 0)
          local CurrentHeading = GetEntityHeading(door)
          local coords = GetEntityCoords(door)

          TriggerServerEvent('InteractSound_SV:PlayWithinDistance', 20, 'vaultopen', 1.0)
        	TriggerServerEvent('esx_bankHeist:CloseVaultServer', hash, coords)
        end

        if CurrentAction == 'terminal2' then
          triggerCombat()
          enterCode(Config.BankHeist.Doors.SecDoor)
        end
      elseif IsControlJustReleased(0, Keys['H']) then
        triggerCombat()
        if CurrentAction == 'terminal1' then
          TriggerServerEvent('esx_bankHeist:hack', Config.BankHeist.Doors.SecDoor2) -- this param is the function deifned above
        elseif CurrentAction == 'terminal2' then
          TriggerServerEvent('esx_bankHeist:hack', Config.BankHeist.Doors.SecDoo) -- this param is the function deifned above
        end
      end

      --CurrentAction = nil
      --CurrentActionData = nil
    end
  end
end)


function enterCode(doorConfig)
  ESX.TriggerServerCallback('esx_bankHeist:getCode', function(Code)
    local EnteredCode = KeyboardInput('Secure System: Enter Code', '', 6, false)
    --print("\nEntered Code: " .. EnteredCode)
    --print("\nActual Code: " .. Code)
    if EnteredCode then
      if tonumber(EnteredCode) == Code then
        drawNotification('~g~The entered Code is correct!')
        PlaySoundFrontend(-1, "Hack_Success", "DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS", 1)

        local door = GetClosestObjectOfType(doorConfig.Pos.x, doorConfig.Pos.y, doorConfig.Pos.z, 25.0, doorConfig.Hash, 0, 0, 0)
        local CurrentHeading = GetEntityHeading(door)
        local coords = GetEntityCoords(door)
        local hash =  doorConfig.Hash
        print("\nhash: " .. hash)

        if hash == Config.BankHeist.Doors.VaultDoor.Hash then --vault door
          if 160.0 - round(CurrentHeading, 1) < 10 then -- the door's angle is close to 160 when closed
            --TriggerServerEvent('esx_bankHeist:UnlockDoorServer', hash, coords, door)
            TriggerServerEvent('InteractSound_SV:PlayWithinDistance', 20, 'vaultopen', 1.0)
          	TriggerServerEvent('esx_bankHeist:OpenVaultServer', hash, coords)
          end
        elseif hash == Config.BankHeist.Doors.SecDoor.Hash then -- security door
          if 250.0 - round(CurrentHeading, 1) < 10 then -- the door's angle is close to 250.0 when closed
            TriggerServerEvent('esx_doorlock:updateState', 20, false)
            --TriggerServerEvent('esx_bankHeist:UnlockDoorServer', hash, coords, door)
            if PlayerData.job.name ~= 'banker' and PlayerData.job.name ~= 'police' then
      				triggerCombat()
            end
          end
        end
      else
        drawNotification('~r~The entered Code is not correct!')
        PlaySoundFrontend(-1, "Hack_Failed", "DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS", 1)

        failCount = failCount + 1
    		if failCount == 3 then
    			alertPolice()
          SetPlayerWantedLevel(PlayerId(), 4, false)
          SetPlayerWantedLevelNow(PlayerId(), false)
    		end
      end
    end
  end)
end


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	Citizen.Wait(5000)
	TriggerServerEvent('esx_policejob:forceBlip')
end)

AddEventHandler('onResourceStop', function(resource)
  if resource ~= GetCurrentResourceName() then return end

  local door = Config.BankHeist.Doors.VaultDoor
  local hash =  door.Hash
  local door  = GetClosestObjectOfType(door.Pos.x, door.Pos.y, door.Pos.z, 10.0, hash, 0, 0, 0)
  local coords = GetEntityCoords(door)
  SetEntityHeading(door, 160.0)
  FreezeEntityPosition(door, true)

  clearArea()
  for i=1, #spawnedNpcs, 1 do
    local ped = spawnedNpcs[i][1]
    local coords = spawnedNpcs[i][2]
    --print(ped)
    --print(json.encode(coords))
    SetEntityAsMissionEntity(ped)
    DeleteEntity(ped)
  end
end)

RegisterNetEvent('esx_bankHeist:startHack')
AddEventHandler('esx_bankHeist:startHack', function(doorCfg)
	TriggerEvent("mhacking:show") --This line is where the hacking even starts
	TriggerEvent("mhacking:start",7,19,mycb) --This line is the difficulty and tells it to start. First number is how long the blocks will be the second is how much time they have is.
end)

RegisterNetEvent('esx_bankHeist:createGuard')
AddEventHandler('esx_bankHeist:createGuard', function(coords, h)
  local hash = GetHashKey("mp_s_m_armoured_01")

  while not HasModelLoaded(hash) do
    RequestModel(hash)
    Wait(20)
  end

  if NetworkIsHost() then
    --local doesAlreadyExist = DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 2.0, hash, 0)
    --if not doesAlreadyExist then
      local ped = CreatePed(6, hash, coords.x, coords.y, coords.z, h, true, true)
      SetEntityAsMissionEntity(ped)
      SetBlockingOfNonTemporaryEvents(ped, false)
      SetPedAlertness(ped, 2)
      TaskStandGuard(ped, coords.x, coords.y, coords.z, h, "WORLD_HUMAN_GUARD_STAND")
      --SetPedAsEnemy(ped, true)
      SetPedCombatAbility(ped, 0)
      SetPedCombatAttributes(ped, 46, true)
      SetPedFleeAttributes(ped, 0, 0)
      GiveWeaponToPed(ped, GetHashKey("weapon_pistol"), 20, false)
      --SetPedCanBeTargetted(ped, false)
      SetPedRelationshipGroupHash(ped, GetHashKey("SECURITY_GUARD"))
      table.insert(spawnedNpcs,{ped,coords}) -- wrong number of args
      Wait(500)
    --end
  end
end)

AddEventHandler('esx_bankHeist:hasEnteredMarker', function(part)
  if part == 'resetAlarm' then
    CurrentAction     = 'reset_alarm'
    CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to reset alarm'
  end

  if part == 'silentAlarm' then
    CurrentAction     = 'silent_alarm'
    CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to trigger silent alarm'
  end

  if part == 'takemoney' then
    CurrentAction     = 'take_money'
    CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to take money'
  end

  if part == 'managerpc' then
    if PlayerData.job.name == 'banker' and PlayerData.job.grade > 3 then--and PlayerData.job.grade > 3 then
      CurrentAction     = 'manager_pc'
      CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to lookup code'
    else
      CurrentAction     = 'manager_pc_hack'
      CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to start hacking'
    end
  end

  if part == 'terminal1' then --terminal downstairs
    local VaultDoor = Config.BankHeist.Doors.VaultDoor
    local VaultDoor = GetClosestObjectOfType(VaultDoor.Pos.x, VaultDoor.Pos.y, VaultDoor.Pos.z, 25.0, VaultDoor.Hash, 0, 0, 0)
    local CurrentHeading = GetEntityHeading(VaultDoor)
    if 160.0 - round(CurrentHeading, 1) < 10 then
      ESX.TriggerServerCallback('esx_bankHeist:hasKeycard', function(hasKeycard)
        if not hasKeycard then
          CurrentAction     = 'terminal1'
          CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to enter the Security Access Code'
          CurrentActionData = Config.BankHeist.Doors.VaultDoor
          setCurrentAction(CurrentAction, CurrentActionMsg, CurrentActionData)
        else
          CurrentAction     = 'terminal1'
          CurrentActionMsg  = 'Press ~y~E~s~ to enter the ~g~Security Access Code~s~ or ~y~H~s~ to start ~r~Hacking~s~'
          CurrentActionData = Config.BankHeist.Doors.VaultDoor
          setCurrentAction(CurrentAction, CurrentActionMsg, CurrentActionData)
        end
      end)
    else
      CurrentAction     = 'terminal1_close'
      CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to close the door'
      CurrentActionData = Config.BankHeist.Doors.VaultDoor
    end
  elseif part == 'terminal2' then --terminal upstairs
    ESX.TriggerServerCallback('esx_bankHeist:isDoorUnlocked', function(doorLocked)
      if not doorLocked then
        ESX.TriggerServerCallback('esx_bankHeist:hasKeycard', function(hasKeycard)
          if not hasKeycard then
            CurrentAction     = 'terminal2'
            CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to enter the Security Access Code'
            CurrentActionData = Config.BankHeist.Doors.SecDoor
            setCurrentAction(CurrentAction, CurrentActionMsg, CurrentActionData)
          else
            CurrentAction     = 'terminal2'
            CurrentActionMsg  = 'Press ~y~E~s~ to enter the ~g~Security Access Code~s~ or ~y~H~s~ to start ~r~Hacking~s~'
            CurrentActionData = Config.BankHeist.Doors.SecDoor
            setCurrentAction(CurrentAction, CurrentActionMsg, CurrentActionData)
          end
        end)
      else
        CurrentAction     = 'lock_door'
        CurrentActionMsg  = 'Press ~INPUT_CONTEXT~ to close the door'
        CurrentActionData = Config.BankHeist.Doors.SecDoor
      end
    end, SecDoor)
  end
  --print("\nCurrentAction: " .. CurrentAction)
end)


AddEventHandler('esx_bankHeist:hasExitedMarker', function(part)
  CurrentAction = nil
end)


RegisterNetEvent('esx_bankHeist:getCode')
AddEventHandler('esx_bankHeist:getCode', function(Code)
  Code = Code
end)

RegisterNetEvent('esx_bankHeist:killblip')
AddEventHandler('esx_bankHeist:killblip', function()
  RemoveBlip(blipRobbery)
end)

RegisterNetEvent('esx_bankHeist:setblip')
AddEventHandler('esx_bankHeist:setblip', function(coords)
  blipRobbery = AddBlipForCoord(coords.x, coords.y, coords.z)
  SetBlipSprite(blipRobbery , 161)
  SetBlipScale(blipRobbery , 2.0)
  SetBlipColour(blipRobbery, 3)
  PulseBlip(blipRobbery)
end)



RegisterNetEvent('esx_bankHeist:triggerAlarm')
AddEventHandler('esx_bankHeist:triggerAlarm', function(coords)
  RequestScriptAudioBank("SCRIPT/ALARM_BELL_02", true)
  Citizen.Wait(500)

  local AlarmSoundId = GetSoundId()
  PlaySoundFromCoord(AlarmSoundId, "Bell_02", coords.x, coords.y, coords.z, "ALARMS_SOUNDSET", 1, 250, 0)
  ReleaseScriptAudioBank("SCRIPT/ALARM_BELL_02")
  robInProgress = true
end)

RegisterNetEvent('esx_bankHeist:killAlarm')
AddEventHandler('esx_bankHeist:killAlarm', function()
  StopSound(AlarmSoundId)
  ReleaseSoundId(AlarmSoundId)
  AlarmSoundId = nil
  robInProgress = false
end)


RegisterNetEvent('esx_bankHeist:startHacking')
AddEventHandler('esx_bankHeist:startHacking', function()
  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(GetPlayerPed(-1), scenario, 0, false)

  local ped = GetPlayerPed(-1)
  local oldCoords = GetEntityCoords(ped)
  local duration = 30
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Citizen.Wait(0)
  end

  while failed == false do
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      ESX.ShowHelpNotification("The action was cancelled")
      --ESX.ShowHelpNotification(_U('action_cancelled'))
      ClearPedTasksImmediately(GetPlayerPed(-1))
      failed = true
      break
    end

    if timer == duration then
      TriggerServerEvent('esx_bankHeist:hackingDone')
      ClearPedTasksImmediately(GetPlayerPed(-1))
      break
    end

    Wait(1000)
  end
end)

RegisterNetEvent('esx_bankHeist:heistStart')
AddEventHandler('esx_bankHeist:heistStart', function()
  SetPlayerWantedLevel(PlayerId(), 4, false)
  SetPlayerWantedLevelNow(PlayerId(), false)

  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(GetPlayerPed(-1), scenario, 0, false)

  local ped = GetPlayerPed(-1)
  local oldCoords = GetEntityCoords(ped)
  local duration = 10
  local timer = 0
  local failed = false


  while IsPedUsingScenario(ped,scenario) == false do
    Citizen.Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
			isDoingAction = false
      ESX.ShowNotification("~r~The action was cancelled")
      ClearPedTasksImmediately(PlayerPedId())
      failed = true
      break
    end

    if timer == duration then
    	isDoingAction = false
      ESX.TriggerServerCallback('esx_bankHeist:canTakeMoney', function(canTake)
        if canTake then
          TriggerServerEvent('esx_bankHeist:takeMoney')
          PlaySoundFrontend(-1, "ROBBERY_MONEY_TOTAL", "HUD_FRONTEND_CUSTOM_SOUNDSET", 1)
        else
          ClearPedTasksImmediately(GetPlayerPed(-1))
          ESX.ShowNotification("~r~There's no more money to take!")
        end
      end)
    end

    Wait(1000)
  end
end)


RegisterNetEvent('esx_bankHeist:OpenVaultClient')
AddEventHandler('esx_bankHeist:OpenVaultClient', function(hash, coords)
  while IsInteriorReady(139265) == false do
    Citizen.Wait(0)
  end

  local Speed =  0.1

	if IsInteriorReady(139265) then
    local VaultDoor = GetClosestObjectOfType(coords.x, coords.y, coords.z, 25.0, hash, 0, 0, 0)

		if VaultDoor ~= nil and VaultDoor ~= 0 then
			while true do
				local CurrentHeading = GetEntityHeading(VaultDoor)
				Citizen.Wait(0)

				if round(CurrentHeading, 1) > 0.0 then -- the door's angle is close to 160 when closed
					SetEntityHeading(VaultDoor, round(CurrentHeading, 1) - Speed) --set the door's angle to 0
				else
					SetEntityHeading(VaultDoor, 0.0)
					FreezeEntityPosition(VaultDoor, true)
          --TriggerServerEvent('esx_bankHeist:LockDoorServer', hash, coords, door)
					break
				end
			end
		end
	end
end)


RegisterNetEvent('esx_bankHeist:CloseVaultClient')
AddEventHandler('esx_bankHeist:CloseVaultClient', function(hash, coords)
  --print("\nhash: " .. hash .. " coords: " .. coords)

  while IsInteriorReady(139265) == false do
    Citizen.Wait(0)
  end

  local Speed =  0.1

	if IsInteriorReady(139265) then
		--print("CloseVaultClientINTERIORREADY")
    local VaultDoor = GetClosestObjectOfType(coords.x, coords.y, coords.z, 25.0, hash, 0, 0, 0)

    while true do
      local CurrentHeading = GetEntityHeading(VaultDoor)
      Citizen.Wait(0)

      if round(CurrentHeading, 1) < 160.0 then -- the door's angle is close to 0 when open
        SetEntityHeading(VaultDoor, round(CurrentHeading, 1) + Speed) --set the door's angle to 160
      else
        SetEntityHeading(VaultDoor, 160.0)
        FreezeEntityPosition(VaultDoor, true)
        --TriggerServerEvent('esx_bankHeist:LockDoorServer', hash, coords, door)
        break
      end
    end
	end
end)


function DisplayHelpText(Text)
	BeginTextCommandDisplayHelp('STRING')
	AddTextComponentSubstringPlayerName(Text)
	EndTextCommandDisplayHelp(0, 0, 1, -1)
end


function drawNotification(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentSubstringPlayerName(text)
	DrawNotification(false, true)
end


function round(num, numDecimalPlaces)
	local mult = 10^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end


function GetIsControlJustReleased(Control)
	if IsControlJustReleased(1, Control) or IsDisabledControlJustReleased(1, Control) then
		return true
	end
	return false
end


function GetIsControlPressed(Control)
	if IsControlPressed(1, Control) or IsDisabledControlPressed(1, Control) then
		return true
	end
	return false
end


function KeyboardInput(TextEntry, ExampleText, MaxStringLenght, NoSpaces)
	AddTextEntry(GetCurrentResourceName() .. '_KeyboardHead', TextEntry)
	DisplayOnscreenKeyboard(1, GetCurrentResourceName() .. '_KeyboardHead', '', ExampleText, '', '', '', MaxStringLenght)
	blockinput = true

	while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
		if NoSpaces == true then
			drawNotification('~y~NO SPACES!')
		end
		Citizen.Wait(0)
	end

	if UpdateOnscreenKeyboard() ~= 2 then
		local result = GetOnscreenKeyboardResult()
		Citizen.Wait(500)
		blockinput = false
		return result
	else
		Citizen.Wait(500)
		blockinput = false
		return nil
	end
end


function Draw(text, r, g, b, alpha, x, y, width, height, layer, center, font)
	SetTextColour(r, g, b, alpha)
	SetTextFont(font)
	SetTextScale(width, height)
	SetTextWrap(0.0, 1.0)
	SetTextCentre(center)
	SetTextDropshadow(0, 0, 0, 0, 0)
	SetTextEdge(1, 0, 0, 0, 205)
	SetTextEntry('STRING')
	AddTextComponentSubstringPlayerName(text)
	Set_2dLayer(layer)
	DrawText(x, y)
end
