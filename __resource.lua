resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX Bank Heist'

version '1.0.1'

client_script {
	'@es_extended/locale.lua',
	'config.lua',
	'locales/en.lua',
	'client/client.lua'
}

server_scripts {
	'@es_extended/locale.lua',
	'config.lua',
	'locales/en.lua',
	'server/server.lua'
}

dependencies {
	'es_extended',
	'esx_jobs',
	'esx_policejob'
}

ui_page 'client/html/index.html'

files {
    'client/html/index.html',
    'client/html/sounds/vaultopen.ogg'
}
