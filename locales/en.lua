Locales['en'] = {
  ['rob_in_prog'] = '~r~%s~s~ in progress at ~y~%s~s~',
  ['cops_needed'] = 'There must be atleast ~y~%i~i~ cops online to do this',
  ['money_stolen'] = 'You stole ~r~$%i~i~',
  ['not_enough_items'] = 'You need atleast ~b~%s~s~ ~y~%s~s~',
  ['action_lockpick_start'] = 'You start lockpicking',
  ['action_lockpick_done'] = 'lockpick successful',
  ['action_cancelled'] = 'The action was cancelled',
  ['get_code'] = 'The code is ~y~%i~i~',
  ['press_to_hack'] = 'press ~y~H~s~ to begin ~r~Hacking~s~',
}
